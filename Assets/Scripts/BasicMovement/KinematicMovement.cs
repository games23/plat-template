﻿using System.Linq;
using UnityEngine;

namespace Assets.Scripts.BasicMovement
{
    public class KinematicMovement : MonoBehaviour
    {
        [SerializeField] float gravityStregth;
        [SerializeField] float walkSpeed;
        [SerializeField] float jumpStrength;
        [SerializeField] int actorHeight;
        [SerializeField] float actorWidth;
        [SerializeField] LayerMask collisionLayer;

        float xVelocity;
        float yVelocity;

        float deltaX => xVelocity * Time.fixedDeltaTime;
        float deltaY => yVelocity * Time.fixedDeltaTime;

        void FixedUpdate()
        {
            xVelocity = Input.GetAxisRaw("Horizontal") * walkSpeed;
            yVelocity -= gravityStregth;

            if (HasWall(xVelocity, Mathf.Abs(deltaX)))
                xVelocity = 0;

            if (IsFallingOrStanding(yVelocity) && IsGrounded(Mathf.Abs(deltaY)))
            {
                yVelocity = 0;
                CorrectYOffest();
            }

            var movementVector = new Vector3(deltaX, deltaY) ;

            transform.Translate(movementVector);
        }

        void CorrectYOffest()
        {
            var hit = Physics2D.Raycast(transform.position, Vector2.down, collisionLayer);
            var pos = transform.position;
            pos.y = hit.point.y + actorHeight/2f;
            transform.position = pos;
        }

        bool IsFallingOrStanding(float vertical) => vertical <= 0;

        bool IsGrounded(float lookAhead)
        {
            var rays = new[]
            {
                //new Ray2D(transform.position + new Vector3(-actorWidth / 2f, 0), Vector2.down),
                new Ray2D(transform.position, Vector2.down),
                //new Ray2D(transform.position + new Vector3(actorWidth / 2f, 0), Vector2.down)
            };

            foreach(var r in rays)
                Debug.DrawRay(r.origin, r.direction * (actorHeight / 2f + lookAhead), Color.blue);

            return rays.Any(r => Physics2D.Raycast(r.origin, r.direction, actorHeight / 2f + lookAhead, collisionLayer));
        }

        bool HasWall(float direction, float lookAhead)
        {
            Debug.DrawRay(transform.position, (Vector2.right * direction).normalized * (actorWidth / 2f + lookAhead), Color.red);
            return Physics2D.Raycast(transform.position, Vector2.right * direction, actorWidth / 2f + lookAhead,
                collisionLayer);
        }

        void Update()
        {

            if (Input.GetButtonDown("Jump") && IsGrounded(0))
                yVelocity += jumpStrength;
        }
    }
}
