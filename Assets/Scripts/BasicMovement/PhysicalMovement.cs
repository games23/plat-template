﻿using UnityEngine;

namespace Assets.Scripts.BasicMovement
{
    public class PhysicalMovement : MonoBehaviour
    {
        [SerializeField] Rigidbody2D body;
        [SerializeField] float walkSpeed;
        [SerializeField] float jumpStrength;
        [SerializeField] float maxSpeed;

        void FixedUpdate()
        {
            var horizontal = Input.GetAxisRaw("Horizontal");
            body.AddForce(Vector2.right * horizontal * walkSpeed);

            body.velocity = Vector3.ClampMagnitude(body.velocity, maxSpeed);
        }

        void Update()
        {
            if (Input.GetButtonDown("Jump"))
                body.AddForce(Vector3.up * jumpStrength);
        }
    }
}
