﻿using UnityEngine;

namespace Assets.Scripts.Character
{
    public class CharacterView : MonoBehaviour
    {
        [SerializeField] Animator animator;
        [SerializeField] SpriteRenderer renderer;
        [SerializeField] float speedAnimationFactor;

        public void OnDrop() => animator.SetTrigger("Drop");
        public void OnJump() => animator.SetTrigger("Jump");

        public void SetSideSpeed(float speed)
        {
            animator.SetFloat("SideSpeed", Mathf.Abs(speed) * speedAnimationFactor);
            if (speed != 0)
                renderer.flipX = speed < 0;
        }

        public void SetVerticalSpeed(float speed) => animator.SetFloat("VertSpeed", speed);
    }
}
