﻿using System;
using UnityEngine;

namespace Assets.Scripts.Character
{
    public class Gravity : MonoBehaviour
    {
        [SerializeField] float gravityFactor;

        Action<float> yModify;
        public void Initialize(Action<float> yModify) => 
            this.yModify = yModify;

        public void ApplyGravity() => yModify(-gravityFactor);
    }
}
