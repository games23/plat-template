﻿using System;
using UnityEngine;

namespace Assets.Scripts.Character
{
    public class Jump : MonoBehaviour
    {
        [SerializeField] float jumpStrength;

        Action<float> yModify;

        public void Initialize(Action<float> yModify) => this.yModify = yModify;

        public void NormalJump() => yModify(jumpStrength);
    }
}
