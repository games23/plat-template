﻿using System;
using UnityEngine;

namespace Assets.Scripts.Character
{
    public class Knockback: MonoBehaviour
    {
        Action<float> xSet;
        Action<float> ySet;
        [SerializeField] float knockbackXStrength;
        [SerializeField] float knockbackYStrength;

        public void Initialize(Action<float> xSet, Action<float> ySet)
        {
            this.xSet = xSet;
            this.ySet = ySet;
        }

        public void Do(float xDirection)
        {
            xSet(xDirection * knockbackXStrength);
            ySet(xDirection * knockbackYStrength);
        }
    }
}
