using Assets.Scripts.Character;
using UnityEngine;

public class TurnToGravity : MonoBehaviour
{
    [SerializeField] EnvironmentDetection detector;
    [SerializeField] Transform mainTransform;
    
    void Update()
    {
        var normal = detector.NormalFromGround(mainTransform.position, 10);
        mainTransform.rotation = Quaternion.LookRotation(Vector3.forward, normal);
    }
}
