﻿using System;
using UnityEngine;

namespace Assets.Scripts.Character
{
    public class SideMovement : MonoBehaviour
    {
        [SerializeField] float sideAcceleration;
        [SerializeField] float breakForce;
        [SerializeField] float breakThreshold;

        Action<float> xModify;
        Action<float> xSet;

        public void Initialize(Action<float> xModify, Action<float> xSet)
        {
            this.xModify = xModify;
            this.xSet = xSet;
        }

        public void Move(float xAxis) => 
            xModify(xAxis * sideAcceleration);

        public void ApplyBreak(float xCurrentVelocity) =>
            xSet(Mathf.Abs(xCurrentVelocity) < breakThreshold ? 0 : Mathf.Lerp(xCurrentVelocity, 0, breakForce));
    }
}
