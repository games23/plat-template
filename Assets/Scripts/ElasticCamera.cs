﻿using UnityEngine;

namespace Assets.Scripts
{
    public class ElasticCamera : MonoBehaviour
    {
        [SerializeField] Transform target;
        [SerializeField] float elasticity;

        void FixedUpdate()
        {
            var cameraZ = transform.position.z;
            var newPos = Vector2.Lerp(transform.position, target.position, elasticity);

            transform.position = new Vector3(newPos.x, newPos.y, cameraZ);
        }
    }
}
